﻿### Casus Belli Values
## The costs were decreased for several CB's as there are more counties in Ave Maria, so the costs need to be less.
# Standard value, Vanilla equivalent = 100
am_standard_cb_cost = 50
# Higher value, Vanilla equivalent = 200
am_high_cb_cost = 100